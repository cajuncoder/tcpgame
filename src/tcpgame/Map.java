package tcpgame;

import java.awt.Color;
import java.awt.Graphics;
import java.io.Serializable;
import java.util.Random;

public class Map implements Serializable {
	
	public int tileSize = 8;
	public int nOfTilesX = 36*2;
	public int nOfTilesY = 36*2;
	Integer[][] tiles = new Integer[nOfTilesX][nOfTilesY];
	
	public Map() {
		
		Random random = new Random();
		for(int y = 0; y < nOfTilesY; y++) {
			for(int x = 0; x < nOfTilesX; x++) {
				int tile = 0;
				int r = random.nextInt(30);
				if(r == 0) tile = 1;
				tiles[x][y] = tile;
			}
		}
		
	}
	
	public Map(Integer[][] ingr) {
		for(int y = 0; y < nOfTilesY; y++) {
			for(int x = 0; x < nOfTilesX; x++) {
				tiles[x][y] = ingr[x][y];
			}
		}
	}

	public int getTile(int x, int y){
		if(x >= 0 && y >= 0) {
			x=x/8;
			y=y/8;
			if(x < nOfTilesX && y < nOfTilesY) return tiles[x][y];
		}
		return 1;
	}
	
	public int getTile(double x, double y){
		if(x >= 0 && y >= 0) {
			x=Math.round((float)x)/8;
			y=Math.round((float)y)/8;
			if(x < nOfTilesX && y < nOfTilesY) return tiles[(int)x][(int)y];
		}
		return 1;
	}
	
	public void draw(Graphics g, int xOff, int yOff) {
		for(int y = 0; y < nOfTilesY; y++) {
			for(int x = 0; x < nOfTilesX; x++) {
				if(tiles[x][y] == 0) {
					g.setColor(Color.GRAY.darker().darker());
					g.fillRect((x*tileSize)-xOff, (y*tileSize)-yOff, tileSize, tileSize);
				}else{
					g.setColor(Color.GRAY.darker());
					g.fillRect((x*tileSize)-xOff, (y*tileSize)-yOff, tileSize, tileSize);
					g.setColor(Color.GRAY);
					g.drawRect((x*tileSize)-xOff, (y*tileSize)-yOff, tileSize, tileSize);
				}
			}
		}
	}
}
