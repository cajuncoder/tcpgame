package tcpgame.graphics;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.io.Serializable;

import javax.swing.ImageIcon;


public class SpriteSheet implements Serializable {

	public int spriteSizeX = 0;
	public int spriteSizeY = 0;
	public int spritesX;
	public int spritesY;
	public Image sheet;
	public int sheetWidth;
	public int sheetHeight;
	public Sprite[] sprites;
	
	//----------------Constructor----------------//
	public SpriteSheet(String tileSheet, int spriteSizeX, int spriteSizeY) {
	
		//size
		this.spriteSizeX = spriteSizeX;
		this.spriteSizeY = spriteSizeY;
		
		//load image
		sheet = new ImageIcon(tileSheet).getImage();
		sheetWidth = sheet.getWidth(null);
		sheetHeight = sheet.getHeight(null);
		System.out.println(sheetWidth);

		//store sprite frame coordinates in sprite object.
		spritesX = (sheetWidth)/spriteSizeX;
		spritesY = (sheetHeight)/spriteSizeY;
		sprites = new Sprite[spritesX*spritesY];
		for(int y = 0; y < spritesY; y++) {
			for(int x = 0; x < spritesX; x++) {
				sprites[x + y*spritesX] = new Sprite(x*this.spriteSizeX, y*this.spriteSizeY);
			}
		}
		System.out.println("Sprites loaded: " + sprites.length);
	}
	
	//------------------drawSprite------------------//
	public void drawSprite(int dx, int dy, int spriteIndex, Graphics g) {
		
		//draw sprite from sheet
		g.drawImage(sheet, dx, dy, dx+spriteSizeX, dy+spriteSizeY, sprites[spriteIndex].x, sprites[spriteIndex].y, sprites[spriteIndex].x+spriteSizeX, sprites[spriteIndex].y+spriteSizeY, null);

	}
}
