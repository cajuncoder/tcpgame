package tcpgame;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.font.FontRenderContext;
import java.awt.geom.AffineTransform;

import tcpgame.network.Connection;

public class Scoreboard {

	private Game game;
	public String[] scores = {};
	private String[] oldScores = {};
	private int x = 0;
	private int y = 0;
	private int width = 0;
	private int height = 0;
	private int padding = 4;
	
	AffineTransform affinetransform = new AffineTransform();     
	FontRenderContext frc = new FontRenderContext(affinetransform,true,true); 
	
	public Scoreboard(Game game) {
		this.game = game;
	}
		//(int)(optionFont.getStringBounds(options[i], frc).getWidth());

	
	public void update() {
		if(game.server!=null) {
			scores = new String[game.server.connections.size()];
			for(int i = 0; i < game.server.connections.size(); i++) {
				Connection c = game.server.connections.get(i);
				scores[i] = c.name + " - Kills: " + c.kills + " Deaths: " + c.deaths;
			}
			if(!compare(scores, oldScores)) game.server.sendAll(scores);
			oldScores = scores;
		}
		width = 0; height = 0;
		for(int i = 0; i < scores.length; i++) {
			int w = (int)(game.bufferGraphics.getFont().getStringBounds(scores[i], frc).getWidth());
			int h = (int)(game.bufferGraphics.getFont().getStringBounds(scores[i], frc).getHeight());
			height += h;
			if(w > width) width = w;
		}
	}
	
	public boolean compare(String[] str1, String[] str2) {
		if(str1.length == str2.length) {
			for(int i = 0; i < str1.length; i++) {
				if(!str1[i].equals(str2[i])) return false;
			}
		}else{return false;}
		return true;
	}
	
	public void draw(Graphics g) {
		x = (Game.WIDTH / 2) - (width/2);
		y = (Game.HEIGHT / 2) - (height/2);
		g.setColor(new Color(0,0,0,128));
		g.fillRect(x-padding, y, width + padding*2, height + padding);
		g.setColor(Color.BLACK);
		g.drawRect(x-padding, y, width + padding*2, height + padding);
		
		int lineHeight = 0;
		if(scores.length>0) lineHeight = height/scores.length;
		g.setColor(Color.WHITE);
		for(int i = 0; i < scores.length; i++) {
			g.drawString(scores[i], x, y + (lineHeight*i) + lineHeight);
		}
	}
}
