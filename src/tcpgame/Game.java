package tcpgame;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import tcpgame.chat.Chat;
import tcpgame.input.Keyboard;
import tcpgame.input.Mouse;
import tcpgame.input.MouseMotion;
import tcpgame.network.Client;
import tcpgame.network.Connection;
import tcpgame.network.Server;
import tcpgame.weapons.*;
import tcpgame.network.packets.*;

public class Game implements Serializable {

	// ****************************************************//
	//                     TCP GAME                        //
	// ****************************************************//
	// --------------------GAME-CLASS----------------------//

	// JFrame
	public JFrame jframe = new JFrame();
	JPanel jpanel = new JPanel();

	// Graphics
	//public static int WIDTH = 16 * 21;
	//public static int HEIGHT = 16 * 18;
	public static int WIDTH = 320;
	public static int HEIGHT = 320;
	public static int SCALE = 2;
	public Image bufferImage;
	public Graphics bufferGraphics;
	public Graphics g;
	public Audio audio = new Audio();
	public Chat chat;
	public Scoreboard scoreboard;
	public int cameraX = 0;
	public int cameraY = 0;
	
	public static int MENU = 0;
	public static int GAME = 1;
	public int mode = MENU;

	// Keyboard
	public Keyboard keyboard;
	public Mouse mouse;
	public MouseMotion mouseMotion;
	
	// Multiplayer
	public int userId = 0;
	public String userName = null;
	public Color userColor = Color.BLACK;
	public Server server = null;
	public Client client = null;
	public Player player = null;
	public Color colors[] = {Color.BLUE, Color.RED, Color.MAGENTA, Color.GREEN.darker(), Color.ORANGE.darker(), Color.CYAN, Color.PINK, Color.YELLOW};
	public CopyOnWriteArrayList<Player> players = new CopyOnWriteArrayList<Player>();
	public CopyOnWriteArrayList<Bullet> bullets = new CopyOnWriteArrayList<Bullet>();
	public CopyOnWriteArrayList<Weapon> weapons = new CopyOnWriteArrayList<Weapon>();
	public CopyOnWriteArrayList<Magazine> magazines = new CopyOnWriteArrayList<Magazine>();
	public int thisPlayerBulletCount = 0;
	public int serverWeaponsCount = 0;
	public int serverMagazinesCount = 0;
	private long spawnWeaponTime = 0;
	private boolean spawnWeapons = true;
	private long weaponDespawnTime = 30000;
	private long weaponSpawnTime = 15000;
	
	// Level
	public Map map = null;

	// -----------------Constructor-------------------//
	public Game() {
		
		// JFrame
		jframe.setContentPane(jpanel);
		//jframe.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		jframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		jframe.setResizable(false);
		jframe.setVisible(true);
		// jframe.setFocusable(true);
		
		// JPanel
		jpanel.setPreferredSize(new Dimension(WIDTH * SCALE, HEIGHT * SCALE));
		// jpanel.setFocusable(true);
		// jpanel.requestFocus();
		jframe.pack();
		jframe.setLocationRelativeTo(null);

		// Input
		keyboard = new Keyboard();
		mouse = new Mouse();
		mouseMotion = new MouseMotion();
		jframe.addKeyListener(keyboard);
		jpanel.addMouseListener(mouse);
		jpanel.addMouseMotionListener(mouseMotion);
		jframe.setFocusTraversalKeysEnabled(false);
		// jframe.requestFocus();
		
		// Transparent 16 x 16 pixel cursor image.
		BufferedImage cursorImg = new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB);
		// Create a new blank cursor.
		Cursor blankCursor = Toolkit.getDefaultToolkit().createCustomCursor(
		    cursorImg, new Point(0, 0), "blank cursor");
		// Set the blank cursor to the JFrame.
		jframe.getContentPane().setCursor(blankCursor);	
		
		chat = new Chat(0, HEIGHT-4, 4, keyboard, this);

		jframe.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				//jframe.dispose();
				//serverClient.sendAll("LOGOUT:Goodbye World");
				System.exit(0);
			}
		});
		
		userName = JOptionPane.showInputDialog(jframe, "Enter User Name.");
		String option = JOptionPane.showInputDialog(jframe, "Host Server? y/n");
		//TEMPORARY SERVER
		if(option.equalsIgnoreCase("n")){
			String ip = JOptionPane.showInputDialog("Server IP?");
			int port = Integer.valueOf(JOptionPane.showInputDialog("Server Port?"));
			//String ip = "localhost";
			//int port = 5555;
			client = new Client(this, ip, port);
			client.start();
		}else{
			userId = 0;
			userColor = colors[userId];
			int port = Integer.valueOf(JOptionPane.showInputDialog("Server Port?"));
			//int port = 5555;
			server = new Server(this, port);
			server.start();
			map = new Map();
			addPlayer(new Player(userName, userId, keyboard, this));
		}

	}

	// ------------------Main--------------------//
	public static void main(String[] args) {
		Game game = new Game();
		game.run();
	}

	// -------------------Run--------------------//
	public void run() {
		try{
		scoreboard = new Scoreboard(this);
		
		bufferImage = jpanel.createImage(WIDTH, HEIGHT);
		bufferGraphics = bufferImage.getGraphics();

		// time
		int fps = 0;
		int ticks = 0;
		long time = 0;
		long oldtick = 0;
		long oldfps = 0;
		

		while (true) {
			player = lookUpPlayer(userId);
			// update
			time = System.nanoTime();

			if (time - oldtick >= 1000000000 / 60) {
				oldtick = time;
				ticks++;
				update();
				render();
				fps++;
			}

			if (time - oldfps >= 1000000000) {
				jframe.setTitle("FPS: " + fps + " Updates: " + ticks);
				fps = 0;
				ticks = 0;
				oldfps = time;
			}
		}
		}catch(Exception e) {
			JOptionPane.showMessageDialog(jframe, e.getStackTrace(), e.getClass().getSimpleName(), JOptionPane.ERROR_MESSAGE);
		}
	}

	// -------------------Update---------------------//
	public void update() {
		long curTime = System.currentTimeMillis();
		keyboard.update();
		chat.update();
		scoreboard.update();
		
		if(server!=null && weaponDespawnTime!=0) {
			despawnWeapon();
			despawnMagazine();
		}
		if(server!=null && spawnWeapons && curTime - spawnWeaponTime > weaponSpawnTime) {
			spawnRandomWeapon();
			spawnWeaponTime = curTime;
		}
		
		//for(int p = 0; p < players.size(); p++) {
		//	if(keyboard!=null&&!chat.inputMode) players.get(p).update();
		//}
		if(player!=null && !chat.inputMode) player.update();
		
		for(int b = 0; b < bullets.size(); b++) {
			bullets.get(b).update();
		}
	}

	// -------------------Render---------------------//
	public void render() {

		// if(g == null)
		g = jpanel.getGraphics();

		bufferGraphics.setColor(Color.GRAY);
		bufferGraphics.fillRect(0, 0, WIDTH, HEIGHT);
		// state.draw(bufferGraphics);
		// Image player = new ImageIcon("res/player.png").getImage();
		// bufferGraphics.drawImage(player, WIDTH/2 -8, HEIGHT/2 -16-8,
		// null);
		if(map!=null) map.draw(bufferGraphics, cameraX, cameraY);
		
		for(Weapon w : weapons) {
			w.drawOnGround(bufferGraphics, cameraX, cameraY);
		}
		
		for(Magazine m : magazines) {
			m.drawOnGround(bufferGraphics, cameraX, cameraY);
		}
		
		for(int p = 0; p < players.size(); p++) {
			players.get(p).draw(bufferGraphics, cameraX, cameraY);
		}
		for(int b = 0; b < bullets.size(); b++) {
			bullets.get(b).draw(bufferGraphics, cameraX, cameraY);
		}
		
		if(player!=null) player.drawHUD(bufferGraphics, cameraX, cameraY);
		bufferGraphics.setColor(Color.black);
		chat.draw(bufferGraphics);
		if(keyboard.tab) scoreboard.draw(bufferGraphics);

		//Graphics2D g2d = (Graphics2D) g;
		//g2d.rotate(Math.toRadians(27), WIDTH*SCALE/2, HEIGHT*SCALE/2);
		//g2d.drawImage(bufferImage, 0, 0, WIDTH*SCALE, HEIGHT*SCALE, jpanel);
		g.drawImage(bufferImage, 0, 0, WIDTH * SCALE, HEIGHT * SCALE, jpanel);

		g.dispose();
	}

	
	// ----------------------ADD/REMOVE------------------------///
	public synchronized void addPlayer(Player player) {
		player.spawnedTime = System.currentTimeMillis();
		if(server!=null) {
			for(Object o : server.connections) {
				Connection c = (Connection)o;
				if(c.id == player.id) {
					c.player = player;
					player.connection = c;
					System.out.println(c.name + "added to connection.");
				}
			}
		}
		players.add(player);
	}
	
	public synchronized void addBullet(Bullet bullet) {
		if(bullet.game == null) bullet.game = this;
		bullet.play();
		bullets.add(bullet);
	}
	
	public synchronized void removePlayer(Player player) {
		if(server!=null) {
			for(Connection c : server.connections) {
				if(c.id == player.id) c.player = null;
			}
		}
		players.remove(player);
		audio.play("res/sounds/bulletbody.wav");
	}
	
	public synchronized void removeBullet(Bullet bullet) {
		bullets.remove(bullet);
	}
	
	public synchronized void removeBullet(String str) {
		for(Bullet b : bullets) {
			if(b.bulletId.equals(str)) {
				bullets.remove(b);
			}
		}
	}
	
	public synchronized void removePlayer(int id) {
		for(int p = 0; p < players.size(); p++) {
			if(players.get(p).id == id) players.remove(p);
		}
	} 
	
	public synchronized void updatePlayer(int id, int x, int y, boolean alive) {
		for(Player p : players) {
			if(p.id == id) {
				p.alive = alive;
				p.x = x;
				p.y = y;
				if(p.alive == false) {
					removePlayer(p);
					if(server!=null) p.connection.player = null;
				}
			}
		}
	}
	
	public synchronized void updatePlayerPos(int id, int x, int y) {
		for(Player p : players) {
			if(p.id == id) {
				p.x = x;
				p.y = y;
			}
		}
	}
	
	public synchronized Player lookUpPlayer(int id) {
		Player result = null;
		for(Player p : players){
			if(p.id == id) {
				result = p;
				return p;
			}
		}
		return result;
	}
	
	public synchronized void addWeapon(Weapon weapon) {
		System.out.println("Received Weapon's id is: " + weapon.id);
		if(server!=null && weapon.id == -1) {
			serverWeaponsCount+=1;
			weapon.id = serverWeaponsCount;
			System.out.println("Setting id: " + weapon.id);
		}
		weapon.timeDropped = System.currentTimeMillis();
		weapons.add(weapon);
	}
	
	public synchronized void addMagazine(Magazine magazine) {
		System.out.println("Received Magazine's id is: " + magazine.id);
		if(server!=null && magazine.id == -1 || magazine.id == 0) {
			serverMagazinesCount+=1;
			magazine.id = serverMagazinesCount;
			System.out.println("Setting id: " + magazine.id);
		}
		magazine.timeDropped = System.currentTimeMillis();
		magazines.add(magazine);
	}
	
	public synchronized void removeWeapon(int id) {
		for(Weapon w : weapons) {
			if(w.id == id) weapons.remove(w);
		}
	}
	
	public synchronized void removeMagazine(int id) {
		for(Magazine m : magazines) {
			if(m.id == id) magazines.remove(m);
		}
	}
	
	public void spawnRandomWeapon() {
		if(weapons.size()>12) return;
		int x = -1;
		int y = -1;
		Random random = new Random();
		while(map.getTile(x,y)!=0) {
			x = random.nextInt(map.nOfTilesX*map.tileSize);
			y = random.nextInt(map.nOfTilesY*map.tileSize);
		}
		int type = random.nextInt(3);
		if(type == 0) {
			Rifle rifle = new Rifle(this, null);
			rifle.groundX=x;
			rifle.groundY=y;
			
			Magazine magazine = new RifleMagazine();
			Random r = new Random();
			magazine.groundX = x + (r.nextInt(20)-10);
			magazine.groundY = y + (r.nextInt(20)-10);
			
			addWeapon(rifle);
			addMagazine(magazine);

			server.sendAll(rifle);
			server.sendAll(magazine);
		}
		if(type == 1) {
			SMG smg = new SMG(this, null);
			smg.groundX=x;
			smg.groundY=y;

			Magazine magazine = new SMGMagazine();
			Random r = new Random();
			magazine.groundX = x + (r.nextInt(20)-10);
			magazine.groundY = y + (r.nextInt(20)-10);

			addWeapon(smg);
			addMagazine(magazine);
			
			server.sendAll(smg);
			server.sendAll(magazine);
		}
		if(type == 2) {
			Shotgun shotgun = new Shotgun(this, null);
			shotgun.groundX=x;
			shotgun.groundY=y;

			for(int i = 0; i < 5; i ++) {
				Magazine magazine = new ShotgunShell();
				Random r = new Random();
				magazine.groundX = x + (r.nextInt(20)-10);
				magazine.groundY = y + (r.nextInt(20)-10);
				addMagazine(magazine);
				server.sendAll(magazine);
			}
			
			addWeapon(shotgun);
			server.sendAll(shotgun);
		}
		
	}
	
	public void despawnWeapon() {
		for(Weapon w : weapons) {
			if(System.currentTimeMillis() - w.timeDropped > weaponDespawnTime) {
				RemoveWeaponPacket pkt = new RemoveWeaponPacket(w.id);
				removeWeapon(w.id);
				server.sendAll(pkt);
			}
		}
	}
	
	public void despawnMagazine() {
		for(Magazine m : magazines) {
			if(System.currentTimeMillis() - m.timeDropped > weaponDespawnTime) {
				RemoveMagazinePacket pkt = new RemoveMagazinePacket(m.id);
				removeMagazine(m.id);
				server.sendAll(pkt);
			}
		}
	}
}
