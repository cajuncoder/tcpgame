package tcpgame;
import sun.audio.*;
import java.io.*;

public class Audio {
	AudioPlayer MGP = AudioPlayer.player;
	
	public void play(String file) {
		
		AudioStream BGM;
		AudioData MD; //my data
		//ContinuousAudioDataStream
		AudioDataStream loop = null;

		try{
		BGM = new AudioStream(new FileInputStream(file));
		MD = BGM.getData();
		loop = new AudioDataStream(MD);
		}catch(Exception e){
			System.out.println("Cannot find file!");
		}
		
		MGP.start(loop);
	}
	
	public void play(String file, float volume) {
		
		AudioStream BGM;
		AudioData MD; //my data
		//ContinuousAudioDataStream
		AudioDataStream loop = null;

		try{
		BGM = new AudioStream(new FileInputStream(file));
		MD = BGM.getData();
		loop = new AudioDataStream(MD);
		}catch(Exception e){
			System.out.println("Cannot find file!");
		}
		
		MGP.start(loop);
	}
}
