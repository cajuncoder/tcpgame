package tcpgame.network;

import java.util.List;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.SocketException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
//Thank you Doug Lea. Thank you for this awesome concurrent package.
import java.util.concurrent.CopyOnWriteArrayList;
import tcpgame.network.packets.*;

import tcpgame.Game;

public class Server extends Thread {

	private Connection newUser;
	private Connection newUser2;
	// JOEL: Replace the above with a list of known connections
	//public List<Connection> connections = Collections.synchronizedList(new ArrayList<Connection>());
	public CopyOnWriteArrayList<Connection> connections;
	private Game game;
	private int port;
	private ServerSocket serverSocket;

	public Server(Game game, int port) {
		this.game = game;
		this.port = port;
		try{
			serverSocket = new ServerSocket(port);
			//serverSocket.setSoTimeout(1);
		}catch(Exception e){e.printStackTrace();}

		connections = new CopyOnWriteArrayList<Connection>();
		
		String ip = "";
		try {
			ip = InetAddress.getLocalHost().getHostAddress().toString();
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		game.chat.addLine("Started a TCP/IP server on " + ip + ":" + port);
		
		connections.add(new ServerConnection(game.userName, game));
	}


	public void run() {
		while(true){
			Connection user = new Connection("", game);
			boolean result = user.connect(serverSocket);
			if (!result) {
				System.out.println("Connection failed");
			} else {
				System.out.println("Connection successful");
				user.id = findNextId();
				connections.add(user);
				send(user.id, user);
				System.out.println(connections.size());
			}
			System.out.println("Server loop completed.");
		}
	}

	//testing sync
	public synchronized void sendAll(Object message) {
		//ArrayList<Connection> tempList = (ArrayList<Connection>) connections.clone();
		//for (Connection user : connections) {
		//	user.send(message);
		//}
		for (int i = 1; i < connections.size(); i++) {
			connections.get(i).send(message);
		}
	}
	
	//Syncrhonize this
	public synchronized void sendAllExcept(Object message, Connection c) {
		//ArrayList<Connection> tempList = (ArrayList<Connection>) connections.clone();
		//for (Connection user : connections) {
		//	if(user != c) user.send(message);
		//}
		for (int i = 1; i < connections.size(); i++) {
			Connection user = connections.get(i);
			if(user != c) user.send(message);
		}
	}
	
	//testing sync
	public synchronized void send(Object message, Connection c) {
		connections.get(connections.indexOf(c)).send(message);
	}

	//Leave this synchronized
	public synchronized void removeConnection(Connection c) {
		if(connections.contains(c)) {
			connections.remove(c);
			int i = connections.indexOf(c);
			System.out.println("Removing client " + c.name);
			game.chat.addLine(c.name + " has disconnected.");
			game.server.sendAllExcept(new LogoutPacket(c.name, c.id),c);
			game.removePlayer(c.id);
		}
	}
	
	//This method crashes if site unavailable.
	public String getIpAddress() {
	    URL url = null;
	    BufferedReader in = null;
	    String ipAddress = "";
	    try{
	    	url = new URL("http://bot.whatismyipaddress.com");
	    	in = new BufferedReader(new InputStreamReader(url.openStream()));
	    	ipAddress = in.readLine().trim();
	    }catch(Exception e){
	    	e.printStackTrace();
	    };
	    return ipAddress;
	}
	
	public int findNextId() {
		for(int result = 1; result < connections.size()+1; result++) {
			boolean idUsed = false;
			for(int c = 0; c < connections.size(); c++) {
				if(connections.get(c).id == result) {
					idUsed = true;
				}
			}
			if(idUsed == false) return result;
		}
		return connections.size()+1;
	}
}