package tcpgame.network;

import java.awt.Color;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.SocketException;

import tcpgame.Game;
import tcpgame.Map;
import tcpgame.Player;
import tcpgame.chat.Line;
import tcpgame.weapons.*;
import tcpgame.network.packets.*;

public class Client extends Thread {

	private Game game;
	private String address;
	private int port;
	private Socket socket;
	private boolean connected = false;
	private ObjectInputStream ois;
	private ObjectOutputStream oos;
	
	public Client(Game game, String address, int port) {
		this.game = game;
		this.address = address;
		this.port = port;
		try{
			socket = new Socket(address, port);
			socket.setTcpNoDelay(true);
			oos = new ObjectOutputStream(socket.getOutputStream());
			ois = new ObjectInputStream(socket.getInputStream());
			//socket.setSoTimeout(100);
			game.chat.addLine("Joining server...");
			//send(new Line("LOGIN:" + game.userName, Color.black));
			send(new LoginPacket(game.userName));
			connected = true;
		}catch(Exception e){e.printStackTrace();};
		
	}
	
	public void run() {
		while(connected){
			Object message = receive();
			if(message!=null) parseInput(message);
		}
	}
	
	public Object receive() {
		Object message = null;
		if(ois!=null){
			try {
				message = ois.readUnshared();
			} catch (Exception e) {
				e.printStackTrace();
				game.chat.addLine("Lost connection with Host...");
				System.out.println("Lost connection with Host...");
				connected = false;
				return null;
			}
		}
		return message;
	}
	
	public synchronized void send(Object message) {
		if(oos!=null){
			try {
				//was .writeObject
				oos.writeUnshared(message);
			} catch (Exception e) {
				e.printStackTrace();
				game.chat.addLine("Lost connection with Host...");
				System.out.println("Lost connection with Host...");
				connected = false;
				return;
			}
		}
	}
	
	public void parseInput(Object packet) {
		Object type = packet.getClass();
		
		//LOGIN
		if(type.equals(LoginPacket.class)) {
			LoginPacket pkt = (LoginPacket)packet;
			game.chat.addLine(pkt.name + " has joined the server.");
		}
		
		//LOGOUT
		if(type.equals(LogoutPacket.class)) {
			LogoutPacket pkt = (LogoutPacket)packet;
			game.chat.addLine(pkt.name + " has disconnected.");
			game.removePlayer(pkt.id);
		}
		
		//CHAT LINE
		if(type.equals(Line.class)) {
			Line pkt = (Line)packet;
			game.chat.addLine(pkt);
		}
		
		//MAP
		if(type.equals(Map.class)) {
			Map pkt = (Map)packet;
			this.game.map = pkt;
		}
		
		//INTEGER
		if(type.equals(Integer.class)) {
			Integer pkt = (Integer)packet;
			this.game.userId = pkt;
			this.game.userColor = game.colors[pkt];
		}
		
		//NEW PLAYER
		if(type.equals(NewPlayerPacket.class)) {
			NewPlayerPacket pkt = (NewPlayerPacket)packet;
			Player newPlayer = new Player(pkt.name, pkt.id, null, game);
			if(newPlayer.id == game.userId) newPlayer.keyboard = game.keyboard;
			game.addPlayer(newPlayer);
		}
		
		//UPDATE PLAYER
		if(type.equals(UpdatePacket.class)) {
			UpdatePacket pkt = (UpdatePacket)packet;
			game.updatePlayer(pkt.id, pkt.x, pkt.y, pkt.alive);
		}
		
		//UPDATE POSITION
		if(type.equals(UpdatePosPacket.class)) {
			UpdatePosPacket pkt = (UpdatePosPacket)packet;
			game.updatePlayerPos(pkt.id, pkt.x, pkt.y);
		}
		
		//BULLET PACKET
		if(type.equals(BulletPacket.class)){
			BulletPacket pkt = (BulletPacket)packet;
			if(pkt.type.equals("RifleBullet")){
				Bullet blt = new RifleBullet(pkt.ownerId, pkt.bulletId, pkt.x, pkt.y, pkt.vecX, pkt.vecY, game);
				game.addBullet(blt);
			}
			if(pkt.type.equals("SMGBullet")){
				Bullet blt = new SMGBullet(pkt.ownerId, pkt.bulletId, pkt.x, pkt.y, pkt.vecX, pkt.vecY, game);
				game.addBullet(blt);
			}
			if(pkt.type.equals("ShotgunPellet")){
				Bullet blt = new ShotgunPellet(pkt.ownerId, pkt.bulletId, pkt.x, pkt.y, pkt.vecX, pkt.vecY, game);
				game.addBullet(blt);
			}
		}
		
		if(type.equals(RemoveBulletPacket.class)){
			RemoveBulletPacket pkt = (RemoveBulletPacket)packet;
			game.removeBullet(pkt.bulletId);
		}
		
		//SCORES
		if(type.equals(String[].class)){
			String[] pkt = (String[])packet;
			game.scoreboard.scores = pkt;
			System.out.println("Scores Updated");
		}
		
		//WEAPON (ON GROUND)//
		if(type.equals(Rifle.class) || type.equals(SMG.class) || type.equals(Shotgun.class)) {
			Weapon pkt = (Weapon)packet;
			pkt.loadTransient(game);
			game.addWeapon(pkt);
		}
		
		//REMOVE WEAPON PACKET
		if(type.equals(RemoveWeaponPacket.class)){
			RemoveWeaponPacket pkt = (RemoveWeaponPacket)packet;
			game.removeWeapon(pkt.id);
		}
		
		//GIVE WEAPON PACKET
		if(type.equals(GiveWeaponPacket.class)){
			GiveWeaponPacket pkt = (GiveWeaponPacket)packet;
			if(game.player!=null) {
				game.player.give(pkt.weapon);
			}
		}
		
		//MAGAZINE (ON GROUND)//
		if(type.equals(RifleMagazine.class) || type.equals(SMGMagazine.class) || type.equals(ShotgunShell.class)) {
			Magazine pkt = (Magazine)packet;
			pkt.loadTransient();
			game.addMagazine(pkt);
		}
		
		//REMOVE MAGAZINE PACKET
		if(type.equals(RemoveMagazinePacket.class)){
			RemoveMagazinePacket pkt = (RemoveMagazinePacket)packet;
			game.removeMagazine(pkt.id);
		}
		
		//GIVE MAGAZINE PACKET
		if(type.equals(GiveMagazinePacket.class)){
			GiveMagazinePacket pkt = (GiveMagazinePacket)packet;
			if(game.player!=null) {
				game.player.give(pkt.magazine);
			}
		}
	}
	
}
