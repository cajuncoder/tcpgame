package tcpgame.network.packets;

import java.io.Serializable;

import tcpgame.weapons.Weapon;

public class PickupWeaponPacket implements Serializable {
	public Weapon weapon;
	public PickupWeaponPacket(Weapon weapon) {
		this.weapon = weapon;
	}
}
