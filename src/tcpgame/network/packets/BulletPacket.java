package tcpgame.network.packets;

import java.io.Serializable;

public class BulletPacket implements Serializable {
	public String type;
	public int ownerId;
	public int x;
	public int y;
	public double vecX;
	public double vecY;
	public String bulletId;
	
	public BulletPacket(String type, int ownerId, String bulletId, int x, int y, double vecX, double vecY) {
		this.type = type;
		this.ownerId = ownerId;
		this.x = x;
		this.y = y;
		this.vecX = vecX;
		this.vecY = vecY;
		this.bulletId = bulletId;
	}
	
}
