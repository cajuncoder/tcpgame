package tcpgame.network.packets;

import java.io.Serializable;

public class UpdatePacket implements Serializable {

	/**
	 * 
	 */
	//private static final long serialVersionUID = 1L;
	public String name;
	public int id;
	public boolean alive;
	public int x;
	public int y;
	
	public UpdatePacket(String name, int id, boolean alive, int x, int y) {
		this.name = name;
		this.id = id;
		this.alive = alive;
		this.x = x;
		this.y = y;
	}
	
}
