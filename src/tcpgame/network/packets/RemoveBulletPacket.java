package tcpgame.network.packets;

import java.io.Serializable;

public class RemoveBulletPacket implements Serializable {
	public String bulletId;
	
	public RemoveBulletPacket(String bulletId) {
		this.bulletId = bulletId;
	}
}
