package tcpgame.network.packets;

import java.io.Serializable;
import tcpgame.weapons.Magazine;

public class GiveMagazinePacket implements Serializable {
	public Magazine magazine;
	
	public GiveMagazinePacket(Magazine magazine) {
		this.magazine = magazine;
	}
}
