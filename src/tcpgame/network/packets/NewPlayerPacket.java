package tcpgame.network.packets;

import java.io.Serializable;

public class NewPlayerPacket implements Serializable {

	public String name;
	public int id;
	public int x;
	public int y;

	public NewPlayerPacket(String name, int id, int x, int y) {
		this.name = name;
		this.id = id;
		this.x = x;
		this.y = y;
	}
}
