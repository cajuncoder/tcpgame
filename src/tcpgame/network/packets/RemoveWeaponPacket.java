package tcpgame.network.packets;

import java.io.Serializable;

import tcpgame.weapons.Weapon;

public class RemoveWeaponPacket implements Serializable {
	public int id;
	public RemoveWeaponPacket(int id) {
		this.id = id;
	}
}
