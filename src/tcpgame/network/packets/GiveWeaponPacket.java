package tcpgame.network.packets;

import java.io.Serializable;

import tcpgame.weapons.Weapon;

public class GiveWeaponPacket implements Serializable {
	public Weapon weapon;
	
	public GiveWeaponPacket(Weapon weapon) {
		this.weapon = weapon;
	}
}
