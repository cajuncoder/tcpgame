package tcpgame.network.packets;

import java.io.Serializable;

public class LogoutPacket implements Serializable {
	public String name;
	public int id;
	public LogoutPacket(String name, int id) {
		this.name = name;
		this.id = id;
	}
}
