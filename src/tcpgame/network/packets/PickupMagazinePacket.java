package tcpgame.network.packets;

import java.io.Serializable;

import tcpgame.weapons.Magazine;


public class PickupMagazinePacket implements Serializable {
	public Magazine magazine;
	public PickupMagazinePacket(Magazine magazine) {
		this.magazine = magazine;
	}
}
