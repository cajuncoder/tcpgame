package tcpgame.network.packets;

import java.io.Serializable;

public class UpdatePosPacket implements Serializable {
	public int id;
	public int x;
	public int y;
	
	public UpdatePosPacket(int id, int x, int y) {
		this.id = id;
		this.x = x;
		this.y = y;
	}
}
