package tcpgame.network.packets;

import java.io.Serializable;

import tcpgame.weapons.Weapon;

public class RemoveMagazinePacket implements Serializable {
	public int id;
	public RemoveMagazinePacket(int id) {
		this.id = id;
	}
}
