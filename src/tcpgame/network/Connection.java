package tcpgame.network;

import java.awt.Color;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;

import tcpgame.Game;
import tcpgame.Player;
import tcpgame.chat.Line;
import tcpgame.network.packets.*;
import tcpgame.weapons.*;

public class Connection extends Thread {

	private Game game;
	public String name = null;
	public int id = -1;
	public Player player = null;
	public int kills = 0;
	public int deaths = 0;
	public Socket clientSocket = null;
	public ObjectInputStream ois = null;
	public ObjectOutputStream oos = null;
	public boolean connected = false;

	public Connection(String name, Socket clientSocket, ObjectInputStream ois, ObjectOutputStream oos, Game game){
		this.name = name;
		this.clientSocket = clientSocket;
		this.ois = ois;
		this.oos = oos;
		this.game = game;
	}

	public Connection(String name, Game game){
		this.name = name;
		this.game = game;
	}

	public void run() {
		while(game.server.connections.contains(this)){
			if(connected){
				Object message = receive();
				if(message!=null) parseInput(message);
			}
		}
	}

	// JOEL: I don't think this needs to be synchronized (now)
	public boolean connect(ServerSocket serverSocket){
		try{
			this.clientSocket = serverSocket.accept();
			clientSocket.setTcpNoDelay(true);
			this.oos = new ObjectOutputStream(clientSocket.getOutputStream());
			this.ois = new ObjectInputStream(clientSocket.getInputStream());
			connected = true;
			System.out.println("Connected");
			//if(clientSocket.isConnected() && clientSocket!=null) {
			this.start();
			//}
			return true;
		}catch(Exception e){
			e.printStackTrace();
			connected = false;
			return false;
		}
	}

	//do not synchronize
	public Object receive() {
		Object message = null;
		if(ois!=null){
			try {
				//was readObject()
				message = ois.readUnshared();
			} catch (Exception e) {
				e.printStackTrace();
				game.server.removeConnection(this);
				return null;
			}
		}
		return message;
	}

	//Leave this synchronized!
	public synchronized void send(Object message) {
		if(!clientSocket.isClosed()){
			try {
				oos.writeUnshared(message);
			} catch (Exception e) {
				e.printStackTrace();
				game.server.removeConnection(this);
				return;
			}
		}
	}
	
	private void parseInput(Object packet) {
		Object type = packet.getClass();
		
		//LOGIN
		if(type.equals(LoginPacket.class)){
			LoginPacket pkt = (LoginPacket)packet;
			if(this.name == null || this.name.equals("")) this.name = pkt.name;
			game.chat.addLine(pkt.name + " has joined the server.");
			game.server.sendAllExcept(new LoginPacket(pkt.name), this);
			game.server.send(game.map, this);
			Player player = new Player(name,id,null,game);
			game.addPlayer(player);
			NewPlayerPacket plyrpkt = new NewPlayerPacket(name, id, player.x, player.y);
			game.server.sendAllExcept(plyrpkt,this);
			for(Player p : game.players) {
				plyrpkt = new NewPlayerPacket(p.name,p.id,p.x,p.y);
				game.server.send(plyrpkt, this);
			}
			
		}
		
		//LOGOUT
		if(type.equals(LogoutPacket.class)){
			LogoutPacket pkt = (LogoutPacket)packet;
		}
		
		//CHAT LINE
		if(type.equals(Line.class)){
			Line pkt = (Line)packet;
			game.chat.addLine(new Line(pkt.str,pkt.clr));
			game.server.sendAllExcept(pkt, this);
		}
		
		//UPDATE PACKET
		if(type.equals(UpdatePacket.class)){
			UpdatePacket pkt = (UpdatePacket)packet;
			game.updatePlayer(pkt.id, pkt.x, pkt.y, pkt.alive);
			game.server.sendAllExcept(pkt,this);
		}
		
		//UPDATE POS PACKET
		if(type.equals(UpdatePosPacket.class)){
			UpdatePosPacket pkt = (UpdatePosPacket)packet;
			game.updatePlayerPos(pkt.id, pkt.x, pkt.y);
			game.server.sendAllExcept(packet,this);
		}
		
		//BULLET PACKET
		if(type.equals(BulletPacket.class)){
			BulletPacket pkt = (BulletPacket)packet;
			if(pkt.type.equals("RifleBullet")){
				Bullet blt = new RifleBullet(pkt.ownerId, pkt.bulletId, pkt.x, pkt.y, pkt.vecX, pkt.vecY, game);
				game.addBullet(blt);
			}
			if(pkt.type.equals("SMGBullet")){
				Bullet blt = new SMGBullet(pkt.ownerId, pkt.bulletId, pkt.x, pkt.y, pkt.vecX, pkt.vecY, game);
				game.addBullet(blt);
			}
			if(pkt.type.equals("ShotgunPellet")){
				Bullet blt = new ShotgunPellet(pkt.ownerId, pkt.bulletId, pkt.x, pkt.y, pkt.vecX, pkt.vecY, game);
				game.addBullet(blt);
			}
			game.server.sendAllExcept(pkt, this);
		}
		
		//WEAPON (ON GROUND)
		if(type.equals(Rifle.class) || type.equals(SMG.class) || type.equals(Shotgun.class)) {
			Weapon pkt = (Weapon)packet;
			pkt.loadTransient(game);
			game.addWeapon(pkt);
			game.server.sendAll(packet);
		}
		
		//REMOVE WEAPON PACKET
		if(type.equals(RemoveWeaponPacket.class)){
			RemoveWeaponPacket pkt = (RemoveWeaponPacket)packet;
			game.removeWeapon(pkt.id);
			game.server.sendAll(pkt);
		}
		
		//PICK UP WEAPON REQUEST PACKET
		if(type.equals(PickupWeaponPacket.class)){
			PickupWeaponPacket pkt = (PickupWeaponPacket)packet;
			
			Weapon result = null;
			for(Weapon w : game.weapons) {
				if(w.id == pkt.weapon.id) result = w;
			}
			
			if(result!=null) {
				RemoveWeaponPacket remove = new RemoveWeaponPacket(result.id);
				//GiveWeaponPacket(String type, boolean magInGun, int roundsInMag, int id)
				GiveWeaponPacket give = new GiveWeaponPacket(result);
				game.removeWeapon(result.id);
				game.server.sendAll(remove);
				game.server.send(give, this);
			}
		}
		
		//MAGAZINE (ON GROUND)
		if(type.equals(RifleMagazine.class) || type.equals(SMGMagazine.class) || type.equals(ShotgunShell.class)) {
			Magazine pkt = (Magazine)packet;
			pkt.loadTransient();
			game.addMagazine(pkt);
			game.server.sendAll(packet);
		}
		
		//REMOVE MAGAZINE PACKET
		if(type.equals(RemoveMagazinePacket.class)){
			RemoveMagazinePacket pkt = (RemoveMagazinePacket)packet;
			game.removeMagazine(pkt.id);
			game.server.sendAll(pkt);
		}
		
		//PICK UP MAGAZINE REQUEST PACKET
		if(type.equals(PickupMagazinePacket.class)){
			PickupMagazinePacket pkt = (PickupMagazinePacket)packet;
			
			Magazine result = null;
			for(Magazine m : game.magazines) {
				if(m.id == pkt.magazine.id) result = m;
			}
			
			if(result!=null) {
				RemoveMagazinePacket remove = new RemoveMagazinePacket(result.id);
				//GiveWeaponPacket(String type, boolean magInGun, int roundsInMag, int id)
				GiveMagazinePacket give = new GiveMagazinePacket(result);
				game.removeMagazine(result.id);
				game.server.sendAll(remove);
				game.server.send(give, this);
			}
		}
		
		
	}
}
