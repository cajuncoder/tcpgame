package tcpgame.network;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

import tcpgame.Game;
import tcpgame.Player;

public class ServerConnection extends Connection {

	public ServerConnection(String name, Game game){
		super(name, game);
		super.clientSocket = null;
		super.ois = null;
		super.oos = null;
		super.id = 0;
	}
	
	//@Override
	public boolean connect(ServerSocket serverSocket){
		return false;
	}
	
	@Override
	public synchronized void send(Object message) {
		System.out.println("Ignoring send@ServerConnection.send()");
	}
}
