package tcpgame.chat;

import java.awt.Color;
import java.io.Serializable;

public class Line implements Serializable {

	public String str = null;
	public Color clr = Color.white;
	public transient long time = -1;
	
	public Line(String str, Color clr){
		this.clr = clr;
		this.str = str;
		this.time = System.currentTimeMillis();
	}
	
	public Line(String str) {
		this.str = str;
	}
	
}
