package tcpgame.chat;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.util.ArrayList;
//Thank you Doug Lea. Thank you for this awesome concurrent package.
import java.util.concurrent.CopyOnWriteArrayList;

import tcpgame.Game;
import tcpgame.input.Keyboard;

public class Chat {

	private Game game;
	
	int x;
	int lowerY;
	int numOfLines;
	int textSize = 15;
	Keyboard keyboard;
	Color chatColor = Color.WHITE;
	Font font = new Font("Console", Font.BOLD, 11);
	
	CopyOnWriteArrayList<Line> lines = new CopyOnWriteArrayList<Line>();
	public String inputLine = "";
	public boolean inputMode = false;
	boolean oldInputMode = false;
	
	public Chat(int x, int lowerY, int numOfLines, Keyboard keyboard, Game game) {
		this.x = x;
		this.lowerY = lowerY;
		this.keyboard = keyboard;
		this.game = game;
		this.numOfLines = numOfLines;
	}
	
	public void update(){
		if(!inputMode){
			if(keyboard.keyCodeTyped == 10) {
				inputMode = true;
			}
		}
		if(inputMode){
			
			if(keyboard.keyCodeTyped != 32) {
				inputLine+=keyboard.keyTyped.trim();
			}else{inputLine+=keyboard.keyTyped;}
			
			if(keyboard.keyCodeTyped == 8 && inputLine.length() > 0) inputLine = inputLine.substring(0, inputLine.length()-1);
			if(keyboard.keyCodeTyped == 10 && inputLine.length() > 0) {
				String n = game.userName + ": ";
				addLine(new Line(n + inputLine, game.userColor));
				
				//game.serverClient.sendAll("CHAT:" + inputLine.trim());
				if(game.server!=null){
					game.server.sendAll(new Line(n + inputLine, game.userColor));
				}
				if(game.client!=null){
					game.client.send(new Line(n + inputLine, game.userColor));
				}
				inputLine = "";
				inputMode = false;
			}
			if(keyboard.keyCodeTyped == 10 && inputLine.length() == 0 && oldInputMode == true){
				inputMode = false;
			}
		}
		oldInputMode = inputMode;
	}
	
	public synchronized void addLine(String string) {
		if(!string.equals(null)) lines.add(new Line(string, chatColor));
		while(lines.size()>numOfLines){
			lines.remove(0);
		}
	}
	
	public synchronized void addLine(Line line) {
		if(!line.equals(null)) {
			line.time = System.currentTimeMillis();
			lines.add(line);
		}
		while(lines.size()>numOfLines){
			lines.remove(0);
		}
	}
	
	public synchronized void addLine(String string, Color clr) {
		if(!string.equals(null)) lines.add(new Line(string, clr));
		while(lines.size()>numOfLines){
			lines.remove(0);
		}
	}
	
	public synchronized void draw(Graphics g){
		long curtime = System.currentTimeMillis();
		g.setFont(font);
		g.setColor(chatColor);
		if(lines.size()>0){
			for(int l = lines.size()-1; l >= 0; l--){
				if(lines.get(l).clr != null){
					g.setColor(lines.get(l).clr);
				}else{g.setColor(chatColor);}
				if(curtime - lines.get(l).time < 7000 || inputMode) {
					Color clr = lines.get(l).clr;
					if(curtime - lines.get(l).time > 6800 && !inputMode) g.setColor(new Color(clr.getRed(), clr.getGreen(), clr.getBlue(), 128));
					g.drawString(lines.get(l).str, x, lowerY-(((lines.size() - l)*textSize)));	
				}
			}
			g.setColor(game.userColor);
			if(inputMode) {
			g.drawString(inputLine+"_", x, lowerY);
			}else{g.drawString(inputLine+"", x, lowerY);}
		}
	}
	
}

