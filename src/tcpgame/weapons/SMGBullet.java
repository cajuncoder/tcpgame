package tcpgame.weapons;

import java.io.Serializable;
import java.util.Random;

import tcpgame.Game;

public class SMGBullet extends Bullet implements Serializable {
	

	
	public SMGBullet(int x, int y, double targetX, double targetY, Game game, int ownerId) {
		super.speed = 4.3;
		super.inaccuracy = 0.14;
		super.audio = "res/sounds/mac10.wav";
		
		Random random = new Random();
		
		this.x = x;
		this.y = y;
		this.xd = x;
		this.yd = y;
		
		double xdist = Math.abs((double)targetX - xd);
		double ydist = Math.abs((double)targetY - yd);
		double dist = xdist+ydist;
		
		double r = ((double)random.nextDouble()*inaccuracy)-(inaccuracy/2);
		targetX += r*dist;
		double xdiff = ((double)targetX) - xd;
		r = ((double)random.nextDouble()*inaccuracy)-(inaccuracy/2);
		targetY += r*dist;
		double ydiff = ((double)targetY) - yd;

		this.vecX = xdiff/(Math.abs(xdiff)+Math.abs(ydiff));
		this.vecY = ydiff/(Math.abs(xdiff)+Math.abs(ydiff));
		
		this.game = game;
		this.ownerId = ownerId;
		game.thisPlayerBulletCount += 1;
		this.bulletId = game.userName + game.thisPlayerBulletCount;
	}
	
	public SMGBullet(int ownerId, String bulletId, int x, int y, double vecX, double vecY, Game game) {
		super.speed = 4.3;
		super.inaccuracy = 0.14;
		super.audio = "res/sounds/mac10.wav";
		
		this.x = x;
		this.y = y;
		this.xd = x;
		this.yd = y;
		
		this.vecX = vecX;
		this.vecY = vecY;
		
		this.game = game;
		this.ownerId = ownerId;
		
		this.bulletId = bulletId;
	}
	
	public void play() {
		game.audio.play(this.audio);
	}
}
