package tcpgame.weapons;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Font;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.CopyOnWriteArrayList;

import tcpgame.Game;
import tcpgame.Player;
import tcpgame.graphics.SpriteSheet;
import tcpgame.network.packets.BulletPacket;

public class Shotgun extends Weapon implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private ArrayList<Magazine> mags = new ArrayList<Magazine>();
	public int rateOfFire = (int) (1000 / 1.8);
	public double recoil = 0.064;
	public boolean fullAuto = false;
	private long lastShot = 0;
	private int timeToReload = 600;
	private long reloadTime = 0;
	private boolean reloading = false;
	private boolean oldR = false;
	private boolean oldClickL = false;
	private int shellCapacity = 5;
	//public int groundX = 0;
	//public int groundY = 0;

	public Shotgun(Game game, Player player) {
		super.game = game;
		super.player = player;
		super.sprite = new SpriteSheet("res/graphics/shotgun.png", 49, 14);
		super.groundSprite = new SpriteSheet("res/graphics/shotgunground.png", 15, 4);
		super.magazineType = ShotgunShell.class;
		//super.magInGun = new RifleMagazine();
		super.shellsInGun = new CopyOnWriteArrayList<Magazine>();
		for(int i = 0; i < shellCapacity ; i++) {
			super.shellsInGun.add(new ShotgunShell());
		}
	}

	public void update(int x, int y, int targetX, int targetY) {
		long curTime = System.currentTimeMillis();
		mags.clear();
		for (int i = 0; i < player.mags.size(); i++) {
			Object m = player.mags.get(i);
			if (m.getClass().equals(ShotgunShell.class)) {
				mags.add((ShotgunShell) m);
			}
		}

		if (player.weapons.size() > 0) {
			if (player.weapons.get(0) == this) {

				if (game.keyboard.r && !oldR && reloading == false) {
					reloading = true;
					reloadTime = curTime;
					game.audio.play("res/sounds/insertshell.wav");
				}
				if (reloading && curTime - reloadTime > timeToReload) {
					if (mags.size() > 0) {
						int rnds = 0;
						if (shellsInGun.size() < shellCapacity) {
							Magazine m = mags.get(0);
							shellsInGun.add(m);
							m.inGun = true;
							player.mags.remove(m);
							//if(mags.size()==0) magInGun=null;
						} else {
							//magInGun.inGun = false;
							//player.mags.add(magInGun);
							//Magazine m = mags.get(0);
							//magInGun = m;
							//magInGun.inGun = true;
							//player.mags.remove(m);
							//if(mags.size()==0) magInGun=null;
						}
						//game.audio.play("res/sounds/akboltforward.wav");
						reloading = false;
					}else{
						//if(magInGun!=null){
						//	if(magInGun.rounds>0) player.mags.add(magInGun);
						//}
						//magInGun = null;
						reloading = false;
					}
				}
				
				oldR = game.keyboard.r;

				if (!shellsInGun.isEmpty()) {
					if (curTime - lastShot > rateOfFire
							&& curTime - player.spawnedTime > 2000
							&& shellsInGun.size() > 0 && !reloading) {
						if (game.mouse.clickL && !oldClickL) {
							double[] AimVec = getAimVec();
							double AimX = AimVec[0];
							double AimY = AimVec[1];
							for(int i = 0; i < 5; i++) {
								Bullet bullet = new ShotgunPellet(x, y, targetX
									+ AimX, targetY + AimY, game, player.id);
								game.addBullet(bullet);
								
								BulletPacket bulletpkt = new BulletPacket("ShotgunPellet", bullet.ownerId, bullet.bulletId, bullet.x, bullet.y, bullet.vecX, bullet.vecY);
								if (game.server != null)
									game.server.sendAll(bulletpkt);
								if (game.client != null)
									game.client.send(bulletpkt);
								
							}
							game.audio.play("res/sounds/SHOTGUNRELOAD2.WAV");
							shellsInGun.remove(0);
							
							if (player.accPenalty < 0.8)
								player.accPenalty += recoil;
							lastShot = curTime;

							if (shellsInGun.size() == 0)
								game.audio.play("res/sounds/openbolt.wav");
						} else {
							if (player.accPenalty > 0)
								player.accPenalty -= 0.02;
							if (player.accPenalty < 0)
								player.accPenalty = 0;
						}
						oldClickL = game.mouse.clickL;
					}
				}
			}
		}
	}

	public double[] getAimVec() {
		int targetX = player.targetX;
		int targetY = player.targetY;
		Random random = new Random();
		double inaccuracy = player.accPenalty + player.mvPenalty;

		double xd = player.x;
		double yd = player.y;

		double xdist = Math.abs((double) targetX - xd);
		double ydist = Math.abs((double) targetY - yd);
		double dist = xdist + ydist;

		double x = ((double) random.nextDouble() * inaccuracy)
				- (inaccuracy / 2);
		x = x * dist;
		// double xdiff = ((double)targetX) - xd;
		double y = ((double) random.nextDouble() * inaccuracy)
				- (inaccuracy / 2);
		y = y * dist;
		// double ydiff = ((double)targetY) - yd;

		// double AimVecX = xdiff/(Math.abs(xdiff)+Math.abs(ydiff));
		// double AimVecY = ydiff/(Math.abs(xdiff)+Math.abs(ydiff));

		return new double[] { x, y };
	}

	@Override
	public void draw(Graphics g, int y) {
		// if(inMag > 0) {g.setColor(Color.WHITE);}else{g.setColor(Color.RED);}
		// g.drawString("SMG: " + inMag + "/" + player.smgAmmo,
		// Game.WIDTH/2-(8*4), 10);
		boolean center = false;
		boolean right = false;
		int dx = 0;
		int dy = y;
		
		if (right)
			dx = (Game.WIDTH) - (sprite.spriteSizeX) - (mags.size() * 5) - 5;
		if (center)
			dx = ((Game.WIDTH) - (sprite.spriteSizeX) - (mags.size() * 5) - 5) / 2;

		sprite.drawSprite(dx, dy, 0, g);

		int row = 0; int rowCounter = 0;
		for (int i = 0; i < mags.size(); i++) {
			mags.get(i).draw(g, (dx + sprite.spriteSizeX) + (rowCounter * 5) + 5, (dy+1) + (row*(mags.get(i).sprite.spriteSizeY+1)));
			rowCounter++;
			if(rowCounter >= 5) {
				rowCounter = 0;
				row++;
			}
		}

		//if (magInGun != null && !reloading) {
		//	if (magInGun.inGun != true)
		//		magInGun.inGun = true;
		//	magInGun.draw(g, dx, dy);
		//}
		for (int i = 0; i < shellsInGun.size(); i++) {
			Magazine m = shellsInGun.get(i);
			m.inGun=true;
			m.draw(g, dx-(i*(m.sprite.spriteSizeX+1)), dy);
		}
	}
	
	public void loadTransient(Game game) {
		mags.clear();
		super.game = game;
		super.sprite = new SpriteSheet("res/graphics/shotgun.png", 49, 14);
		super.groundSprite = new SpriteSheet("res/graphics/shotgunground.png", 15, 4);
		for(int i = 0; i < shellsInGun.size() ; i++) {
			super.shellsInGun.get(i).loadTransient();
		}
	}

}