package tcpgame.weapons;

import java.awt.Color;
import java.awt.Graphics;
import java.io.Serializable;
import java.util.Random;

import tcpgame.Game;
import tcpgame.Player;
import tcpgame.chat.Line;
import tcpgame.input.Keyboard;
import tcpgame.network.packets.*;

public abstract class Bullet implements Serializable {
	public int ownerId;
	public String bulletId;
	public transient Game game;
	public double speed;
	public double inaccuracy;
	public int x;
	public int y;
	public int oldx;
	public int oldy;
	public double xd;
	public double yd;
	public double vecX;
	public double vecY;
	public String audio;
	
	public void update() {
		oldx = x;
		oldy = y;
		xd = xd + vecX*speed;
		yd = yd + vecY*speed;
		x = Math.round((float)xd);
		y = Math.round((float)yd);
		collide();
	}
	
	public void collide() {
		if(game.map!=null) {
			if(game.map.getTile(x, y)!=0) {
				
				int xdist = 64; int ydist = 64;
				if(game.player!=null) {
					xdist = Math.abs(game.player.x - this.x);
					ydist = Math.abs(game.player.y - this.y);
				}
				if(xdist < 64 && ydist < 64) game.audio.play("res/sounds/ricochet.wav");
				game.removeBullet(this);
			}
		}else{System.out.println("Map is null");}
		
		long curTime = System.currentTimeMillis();
		for(int i = 0; i < game.players.size(); i++) {
			Player p = game.players.get(i);
			if(p.id != this.ownerId && curTime - p.spawnedTime > 2000) {
				int distX = Math.abs(x-p.x);
				int distY = Math.abs(y-p.y);
				
				if(distX < 5 && distY < 5 && game.server!= null) {
					p.alive = false; p.connection.deaths += 1;
					game.removeBullet(this);
					game.server.sendAll(new RemoveBulletPacket(this.bulletId));
				
					Player killer = game.lookUpPlayer(this.ownerId);
					killer.connection.kills += 1;
					UpdatePacket update = new UpdatePacket(p.name, p.id, p.alive, p.x, p.y);
					game.server.sendAll(update);		
					Line line = new Line(p.name + " was killed by " + killer.name);
					game.chat.addLine(line);
					game.server.sendAll(line);

					Keyboard keyboard = null; if(p.id == game.userId) keyboard = game.keyboard;
					Player player = new Player(p.name, p.id, keyboard, game);
					game.updatePlayer(p.id, p.x, p.y, p.alive);
					game.server.sendAll(new NewPlayerPacket(player.name, player.id, p.x, p.y));
					game.addPlayer(player);
				}
			}
		}
	}
	
	public void draw(Graphics g, int xOff, int yOff) {
		g.setColor(new Color(255,255,0,128));
		//g.fillRect(x-xOff, y-yOff, 1, 1);
		g.drawLine(oldx-xOff, oldy-yOff, x-xOff, y-yOff);
		g.setColor(new Color(255,0,255,255));
		//g.fillRect(x-xOff, y-yOff, 1, 1);
	}
	
	public void play(){
		
	}
}

