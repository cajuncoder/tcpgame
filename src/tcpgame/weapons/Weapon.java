package tcpgame.weapons;

import java.awt.Graphics;
import java.io.Serializable;
import java.util.concurrent.CopyOnWriteArrayList;

import tcpgame.Game;
import tcpgame.Player;
import tcpgame.graphics.SpriteSheet;

public abstract class Weapon implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public transient Game game;
	public int id = -1;
	public Magazine magInGun;
	public CopyOnWriteArrayList<Magazine> shellsInGun;
	public transient Player player;
	public Object magazineType;
	public int groundX;
	public int groundY;
	public transient SpriteSheet sprite;
	public transient SpriteSheet groundSprite;
	public long timeDropped;
	
	public void update(int x, int y, int targetX, int targetY) {
		
	}
	
	public void draw(Graphics g, int index) {
		
	}
	
	public void drawOnGround(Graphics g, int OffX, int OffY) {
		OffX+=groundSprite.spriteSizeX/2;
		OffY+=groundSprite.spriteSizeY/2;
		groundSprite.drawSprite(groundX-OffX, groundY-OffY, 0, g);
	}
	
	public boolean magInGun() {
		if(magInGun!=null) {
			return true;
		}
		return false;
	}
	
	public int rndsInMag() {
		if(magInGun()) return magInGun.rounds;
		return 0;
	}
	
	public void loadTransient(Game game) {
		System.out.println("Empty method @ weapon");
	}
	
}
