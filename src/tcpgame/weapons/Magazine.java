package tcpgame.weapons;

import java.awt.Graphics;
import java.io.Serializable;

import tcpgame.graphics.SpriteSheet;

public abstract class Magazine implements Serializable {
	public int id;
	public int capacity;
	public int rounds;
	public boolean inGun;
	public transient SpriteSheet sprite;
	public transient SpriteSheet groundSprite;
	public int groundX = 0;
	public int groundY = 0;
	public long timeDropped;
	
	public void draw(Graphics g, int x, int y) {
	}
	
	public void drawOnGround(Graphics g, int OffX, int OffY) {
		OffX+=groundSprite.spriteSizeX/2;
		OffY+=groundSprite.spriteSizeY/2;
		groundSprite.drawSprite(groundX-OffX, groundY-OffY, 0, g);
	}
	
	public void loadTransient() {
		System.out.println("Empty method @ magazine");
	}
}
