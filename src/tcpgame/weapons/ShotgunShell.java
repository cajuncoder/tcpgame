package tcpgame.weapons;

import java.awt.Graphics;
import java.io.Serializable;

import tcpgame.graphics.SpriteSheet;

public class ShotgunShell extends Magazine implements Serializable {

	private int gunOffsetX = 28;
	private int gunOffsetY = 3;
	
	public ShotgunShell(){
		super.inGun = false;
		super.sprite = new SpriteSheet("res/graphics/shotgunshell.png", 4, 2);
		super.groundSprite = new SpriteSheet("res/graphics/shotgunshellground.png", 2, 1);
		capacity = 30;
		rounds = 30;
	}
	
	public void draw(Graphics g, int x, int y) {
		if(inGun) {
			x+=gunOffsetX;
			y+=gunOffsetY;
		}
		int i = (sprite.spritesX-1) - (int)Math.floor(((double)sprite.spritesX-0.1)/((double)capacity/(double)rounds));
		sprite.drawSprite(x, y, i, g);
	}
	
	public void loadTransient() {
		super.sprite = new SpriteSheet("res/graphics/shotgunshell.png", 4, 2);
		super.groundSprite = new SpriteSheet("res/graphics/shotgunshellground.png", 2, 1);
	}
}
