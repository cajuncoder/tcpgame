package tcpgame.weapons;

import java.awt.Graphics;
import java.io.Serializable;

import tcpgame.graphics.SpriteSheet;

public class RifleMagazine extends Magazine implements Serializable {

	private int gunOffsetX = 25;
	private int gunOffsetY = 6;
	
	public RifleMagazine(){
		super.sprite = new SpriteSheet("res/graphics/riflemag.png", 6, 11);
		super.groundSprite = new SpriteSheet("res/graphics/riflemagground.png", 3, 4);
		super.inGun = false;
		capacity = 20;
		rounds = 20;
	}
	
	public void draw(Graphics g, int x, int y) {
		if(inGun) {
			x+=gunOffsetX;
			y+=gunOffsetY;
		}
		//if(sprite==null) System.out.println("SPRITE IS NULL");
		int i = (sprite.spritesX-1) - (int)Math.floor(((double)sprite.spritesX-0.1)/((double)capacity/(double)rounds));
		sprite.drawSprite(x, y, i, g);
	}
	
	public void loadTransient() {
		super.sprite = new SpriteSheet("res/graphics/riflemag.png", 6, 11);
		super.groundSprite = new SpriteSheet("res/graphics/riflemagground.png", 3, 4);
	}
}
