package tcpgame.weapons;

import java.awt.Graphics;
import java.io.Serializable;

import tcpgame.graphics.SpriteSheet;

public class SMGMagazine extends Magazine implements Serializable {

	private int gunOffsetX = 10;
	private int gunOffsetY = 6;
	
	public SMGMagazine(){
		super.inGun = false;
		super.sprite = new SpriteSheet("res/graphics/smgmag.png", 4, 12);
		super.groundSprite = new SpriteSheet("res/graphics/smgmagground.png", 2, 5);
		capacity = 30;
		rounds = 30;
	}
	
	public void draw(Graphics g, int x, int y) {
		if(inGun) {
			x+=gunOffsetX;
			y+=gunOffsetY;
		}
		int i = (sprite.spritesX-1) - (int)Math.floor(((double)sprite.spritesX-0.1)/((double)capacity/(double)rounds));
		sprite.drawSprite(x, y, i, g);
	}
	
	public void loadTransient() {
		super.sprite = new SpriteSheet("res/graphics/smgmag.png", 4, 12);
		super.groundSprite = new SpriteSheet("res/graphics/smgmagground.png", 2, 5);
	}
}
