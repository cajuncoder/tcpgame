package tcpgame;

import java.awt.Color;
import java.awt.Graphics;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.CopyOnWriteArrayList;

import tcpgame.input.Keyboard;
import tcpgame.network.Connection;
import tcpgame.network.packets.*;
import tcpgame.weapons.Bullet;
import tcpgame.weapons.Magazine;
import tcpgame.weapons.Rifle;
import tcpgame.weapons.SMG;
import tcpgame.weapons.RifleMagazine;
import tcpgame.weapons.SMGMagazine;
import tcpgame.weapons.Shotgun;
import tcpgame.weapons.ShotgunShell;
import tcpgame.weapons.Weapon;

public class Player implements Serializable {
	
	public transient Game game;
	public boolean alive = true;
	public String name;
	public int id;
	public Connection connection;
	//public Weapon weapon;
	public CopyOnWriteArrayList<Weapon> weapons = new CopyOnWriteArrayList<Weapon>();
	public CopyOnWriteArrayList<Magazine> mags = new CopyOnWriteArrayList<Magazine>();
	private int numOfWeaponsCanCarry = 3;
	private int numOfMagsCanCarry = 99;
	
	//public int smgAmmo = 600;
	public int x = -1;
	public int y = -1;
	public double xd;
	public double yd;
	private double oldx;
	private double oldy;
	public transient int targetX = -1;
	public transient int targetY = -1;
	public double vecX = 0;
	public double vecY = 0;
	public transient Keyboard keyboard = null;
	public transient long lastShot = 0;
	public long spawnedTime = 0;
	public long updateTime = 0;
	public int blinkIt = 0;
	public double mvPenalty = 0;
	public double accPenalty = 0;
	private double walkSpeed = 0.55;
	public int killedByBulletId = -1;
	private boolean oldq = false;
	private boolean oldg = false;

	public Player(String name, int id, Keyboard keyboard, Game game) {
		Random random = new Random();
		this.game = game;
		while(game.map.getTile(this.x,this.y)!=0) {
			this.x = random.nextInt(game.map.nOfTilesX*game.map.tileSize);
			this.y = random.nextInt(game.map.nOfTilesY*game.map.tileSize);
		}
		this.xd = this.x;
		this.yd = this.y;
		this.oldx = xd;
		this.oldy = yd;
		this.name = name;
		this.id = id;
		this.keyboard = keyboard;
		this.spawnedTime = System.currentTimeMillis();
		
		give(new Rifle(this.game, this));
		give(new Shotgun(this.game, this));
		give(new SMG(this.game, this));
		mags.add(new RifleMagazine());
		for(int i = 0; i < 25; i++) {
			mags.add(new ShotgunShell());
		}
		mags.add(new SMGMagazine());
	}
	
	public void update() {
		if(keyboard!=null){
			if(name == null && id == game.userId) name = game.userName;

			long curTime = System.currentTimeMillis();
			//if(curTime-spawnedTime > 2000) alive = true;
			boolean movingVertical = false;
			boolean movingHorizontal = false;
			mvPenalty = 0;
			double speed = walkSpeed;
			
			if(keyboard.up || keyboard.down) {movingVertical = true; mvPenalty = 0.15;}
			if(keyboard.right || keyboard.left) {movingHorizontal = true; mvPenalty = 0.15;}
			if(keyboard.shift) {speed = speed*1.5; mvPenalty = mvPenalty*2.0;}
			if(movingHorizontal && movingVertical) {speed = speed*0.75;}
			
			if(keyboard.up) {yd-=speed; }
			if(keyboard.right) xd+=speed;
			if(keyboard.down) yd+=speed;
			if(keyboard.left) xd-=speed;
			checkCollision();
			x = Math.round((float)xd);
			y = Math.round((float)yd);

			game.cameraX = x - (Game.WIDTH/2);
			game.cameraY = y - (Game.HEIGHT/2);
			
			targetX = (game.mouseMotion.x/Game.SCALE) + game.cameraX;
			targetY = (game.mouseMotion.y/Game.SCALE) + game.cameraY;
			//double xdist = (double)targetX - xd;
			//double ydist = (double)targetY - yd;
			//vecX = xdist/(Math.abs(xdist)+Math.abs(ydist));
			//vecY = ydist/(Math.abs(xdist)+Math.abs(ydist));

			for(Weapon w : weapons) {
				if(w.player == null) w.player = this;
				w.update(x, y, targetX, targetY);
			}
			
			//if(curTime - updateTime > 40) {
				UpdatePosPacket updatePacket = new UpdatePosPacket(id, x, y);
			
				if(game.server!=null) {
					game.server.sendAll(updatePacket);
				}
				if(game.client!=null) {
					game.client.send(updatePacket);
				}
				updateTime = curTime;
			//}
			
			if(keyboard.q && !oldq && weapons.size() > 1) {
				Weapon w = weapons.get(0);
				weapons.remove(w);
				weapons.add(w);
			}
			
			//Nearby Weapons
			ArrayList<Weapon> nearbyWeapons = new ArrayList<Weapon>();
			for(Weapon w : game.weapons) {
				if(Math.abs(w.groundX - x) < 10 && Math.abs(w.groundY - y) < 10){
					nearbyWeapons.add(w);
				}
			}
			//Nearby Magazines
			ArrayList<Magazine> nearbyMagazines = new ArrayList<Magazine>();
			for(Magazine m : game.magazines) {
				if(Math.abs(m.groundX - x) < 10 && Math.abs(m.groundY - y) < 10){
					nearbyMagazines.add(m);
				}
			}
			
			//Pick Up Weapon
			if(keyboard.g && !oldg && nearbyWeapons.size() > 0) {
				if(game.server!=null) {
					for(Weapon w : nearbyWeapons) {
						boolean pickedUp = give(w);
						if(pickedUp) {
							game.weapons.remove(w);
							RemoveWeaponPacket pkt = new RemoveWeaponPacket(w.id);
							game.server.sendAll(pkt);
						}
					}
				}
				if(game.client!=null) {
					boolean[] results = canPickup(nearbyWeapons);
					for(int i = 0; i < results.length; i++) {
						if(results[i]!=false) {
							Weapon w = nearbyWeapons.get(i);
							PickupWeaponPacket pickup = new PickupWeaponPacket(w);
							game.client.send(pickup);
						}
					}
				}
				
			}
			//Pick Up Magazine
			if(keyboard.g && !oldg && nearbyMagazines.size() > 0) {
				if(game.server!=null) {
					for(Magazine m : nearbyMagazines) {
						boolean pickedUp = give(m);
						if(pickedUp) {
							game.magazines.remove(m);
							RemoveMagazinePacket pkt = new RemoveMagazinePacket(m.id);
							game.server.sendAll(pkt);
						}
					}
				}
				if(game.client!=null) {
					boolean[] results = canPickupMagazine(nearbyMagazines);
					for(int i = 0; i < results.length; i++) {
						if(results[i]!=false) {
							Magazine m = nearbyMagazines.get(i);
							PickupMagazinePacket pickup = new PickupMagazinePacket(m);
							game.client.send(pickup);
						}
					}
				}
			}
			//Drop Weapon && Magazines
			if(keyboard.g && !oldg && weapons.size() > 0 && nearbyWeapons.isEmpty() && nearbyMagazines.isEmpty()) {
				Weapon w = weapons.get(0);
				w.groundX = x;
				w.groundY = y;
				if(game.server!=null) {
					game.addWeapon(w);
					//NewWeaponPacket pkt = new NewWeaponPacket(w.getClass().getName(), w.groundX, w.groundY, w.magInGun(), w.rndsInMag(), w.id);
					game.server.sendAll(w);
				}
				if(game.client!=null) {
					//NewWeaponPacket pkt = new NewWeaponPacket(w.getClass().getName(), w.groundX, w.groundY, w.magInGun(), w.rndsInMag(), w.id);
					game.client.send(w);
				}
				weapons.remove(w);
				
				if(!hasWeaponType(w)) {
					for(Magazine m : mags) {
						if(m.getClass().equals(w.magazineType)) {
							Random r = new Random();
							m.groundX = x + (r.nextInt(20)-10);
							m.groundY = y + (r.nextInt(20)-10);
							mags.remove(m);
							if(game.server!=null) {
								game.addMagazine(m);
								game.server.sendAll(m);
							}
							if(game.client!=null) {
								game.client.send(m);
							}
						}
					}
				}
			}
			
			oldg = keyboard.g;
			oldq = keyboard.q;
			oldx = xd;
			oldy = yd;
		}
	}
	
	public void checkCollision() {
		if(game.map.getTile(xd,oldy)!=0) {
			xd = oldx;
		}
		if(game.map.getTile(oldx,yd)!=0) {
			yd = oldy;
		}
	}
	
	public void draw(Graphics g, int xOff, int yOff) {
		Color clr = game.colors[id];
		long curTime = System.currentTimeMillis();
		if(curTime-spawnedTime < 2000) {
			if(blinkIt < 8) clr = new Color(0,0,0,0);
			if(blinkIt > 15) blinkIt = 0;
			blinkIt++;
		}
		
		g.setColor(clr.darker().darker());
		g.fillOval(x-xOff-5, y-yOff-5, 9, 9);
		g.setColor(clr);
		g.fillOval(x-xOff-4, y-yOff-4, 7, 7);
	}
	
	public void drawHUD(Graphics g, int xOff, int yOff) {
		g.setColor(Color.GREEN);
		g.drawOval(targetX-xOff-5, targetY-yOff-5, 10, 10);
		g.drawLine(targetX-xOff, targetY-yOff-6, targetX-xOff, targetY-yOff-3);
		g.drawLine(targetX-xOff, targetY-yOff+6, targetX-xOff, targetY-yOff+3);
		g.drawLine(targetX-xOff-6, targetY-yOff, targetX-xOff-3, targetY-yOff);
		g.drawLine(targetX-xOff+6, targetY-yOff, targetX-xOff+3, targetY-yOff);
		int wdx = 0;
		for(int i = 0; i < weapons.size(); i++) {
			int prevH = 0;
			if(i>0) prevH = weapons.get(i-1).sprite.spriteSizeY + 3;
			wdx += prevH;
			weapons.get(i).draw(g, wdx);
		}
	}
	
	public synchronized boolean give(Weapon weapon) {
		if(weapons.size() < numOfWeaponsCanCarry) {
			weapon.player = this;
			weapon.loadTransient(game);
			weapons.add(weapon);
			return true;
		}
		return false;
	}
	
	private boolean[] canPickup(ArrayList<Weapon> ws) {
		boolean[] results = new boolean[ws.size()];
		for(int i = 0; i < ws.size(); i++) {
			if(this.weapons.size() + i < numOfWeaponsCanCarry) {
				results[i] = true;
			}else{results[i] = false;}
		}
		return results;
	}
	
	private boolean[] canPickupMagazine(ArrayList<Magazine> ws) {
		boolean[] results = new boolean[ws.size()];
		for(int i = 0; i < ws.size(); i++) {
			if(this.mags.size() + i < numOfMagsCanCarry) {
				results[i] = true;
			}else{results[i] = false;}
		}
		return results;
	}
	
	public synchronized boolean give(Magazine magazine) {
		if(mags.size() < numOfMagsCanCarry) {
			magazine.loadTransient();
			mags.add(magazine);
			return true;
		}
		return false;
	}
	
	public boolean hasWeaponType(Weapon weapon) {
		for(Weapon w : weapons) {
			if(w.getClass().equals(weapon.getClass())){
				System.out.println("Found Match in hasWeaponType()");
				return true;
			}
		}
		return false;
	}
	
}
